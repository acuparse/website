# Acuparse.com Website

## New releases

- Copy the last release post in _releases to a new post and edit file name.
- Update front matter with current release.
- Add changelog data to content.
