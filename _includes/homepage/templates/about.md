Acuparse is a PHP/MySQL program that captures, stores, and displays weather data from an AcuRite Iris (5-in-1) or Atlas (
7-in-1) weather station and tower sensors, via your Access/smartHUB. It uploads weather data
to [Weather Underground](https://https://www.wunderground.com), [CWOP](http://www.wxqa.com), [Weathercloud](https://weathercloud.net),
[PWS Weather](https://www.pwsweather.com), [Windy](https://www.windy.com), [Windguru](https://www.windguru.cz),
[OpenWeather](https://openweathermap.org/), and [MQTT](https://mqtt.org/) Brokers. It also processes and stores images from a local network camera for
display and linking to external websites. Acuparse works by intercepting your data on the way to [My AcuRite](https://www.myacurite.com), parsing 
it, and finally sending the original data to [My AcuRite](https://www.myacurite.com) untouched.
