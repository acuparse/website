---
version: '2.9.4'
title: 'Resolve Wind Regression'
date: 2019-08-18
layout: release
permalink: /releases/v2-9-4
image: assets/img/release.jpg
---
### Fixed
- Regression in Wind Direction. Removing Null.
