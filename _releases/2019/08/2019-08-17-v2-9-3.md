---
version: '2.9.3'
title: 'Resolve Lighbox Regression'
date: 2019-08-17
layout: release
permalink: /releases/v2-9-3
image: assets/img/release.jpg
---
### Fixed
- Lightbox not loading
