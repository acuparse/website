---
version: '3.8.0'
title: 'MQTT Support'
date: 2021-10-27
layout: release
permalink: /releases/v3-8-0
image: assets/img/release.jpg
---
### Added

- Ability to publish to an external MQTT Broker.

### Changed

- Offline Towers fail gracefully and will show as offline.
    - API will return `NULL` readings for the offline sensor and `"lastUpdated":"OFFLINE"`.

### Fixed

- Minor changes to lightning logic.
- Small bug fixes and minor code refactoring.
