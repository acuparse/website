---
version: '3.9.4'
title: 'Minor Bug Fixes'
date: 2023-07-12
layout: release
permalink: /releases/v3-9-4
image: assets/img/release.jpg
---
## [3.9.4](https://www.acuparse.com/releases/v3-9-4/) - 2023-07-12

### Fixed

- Updates to the dashboard time logic caused the time not to display.