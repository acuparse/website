---
version: '2.1.8'
title: 'Minor Updates'
date: 2017-12-30
layout: release
permalink: /releases/v2-1-8
image: assets/img/release.jpg
---
### Fixed

- Fixed tower admin not displaying proper privacy status.

### Added

- Added back Weather Underground Camera Upload. WU changed their mind.
