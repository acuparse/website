---
version: '3.9.2'
title: 'Minor Bug Fixes'
date: 2022-11-26
layout: release
permalink: /releases/v3-9-2
image: assets/img/release.jpg
---
### Fixed

- Contact form always enabled when saving system settings.
- Previous cron updates to handle `0` readings with tower data broke external uploads.
- Tower Lightning data not appearing in the API/MQTT.

### Changed

- Bootstrap to 5.2.3.
- FontAwesome to 6.2.1.
- RTL Relay Server to 1.1.0.
  - Syslog port changed to `10514` to avoid conflicts, update your compose files if you are using the RTL relay server.
  - `command: -F syslog:relay:514` to `command: -F syslog:relay:10514`
  - Run `docker-compose pull && docker-compose up -d` to update.

### Added

- Option to use the system timezone for RTL readings.
