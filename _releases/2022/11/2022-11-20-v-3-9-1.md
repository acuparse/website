---
version: '3.9.1'
title: 'Minor Bug Fixes'
date: 2022-11-20
layout: release
permalink: /releases/v3-9-1
image: assets/img/release.jpg
---
### Fixed

- UV Index via RTL was not being processed correctly.

### Changed

- Set retained flag on MQTT messages.
- Update CRON to check for `0` readings before uploading and archiving.
  - When using tower sensors and trimming, readings will zero out when the month rolls over.
