---
version: '2.2.2'
title: 'Access Fixes'
date: 2018-02-26
layout: release
permalink: /releases/v2-2-2
image: assets/img/release.jpg
---
### Added

- Timezone to Access response.
- Script to change the upload server locally on the Access. Removing the DNS redirect requirement.

### Changed

- Updated smartHUB EoL to 2019-03-01 due to AcuRite extending service.

### Fixed

- Wind readings in the archive worded incorrectly.
- Access updates not saving to DB.
