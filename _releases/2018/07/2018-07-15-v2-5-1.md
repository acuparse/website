---
version: '2.5.1'
title: 'Bootstrap Update'
date: 2018-07-15
layout: release
permalink: /releases/v2-5-1
image: assets/img/release.jpg
---
### Changed

- Bootstrap to v4.1.2
- Documentation links to GitHub pages
