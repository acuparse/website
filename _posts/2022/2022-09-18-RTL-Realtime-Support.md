---
layout: post
title: 'v3.9 and Realtime Updates Support'
date: 2022-09-18
---
It's been quite some time since an updated version of Acuparse has been released. There were many changes in progress
including the much requested Realtime update support. Those users with Acurite Access devices know how painful waiting
five minutes between updates can be. With the new Realtime support, you can now get updates as often as every 30 seconds
or so, depending on the device. This is a **beta feature** and has not yet been extensively tested. It has however been
working in production for around a month or so now, and should hopefully work as expected for most of you.

Please drop a note in Slack or on the email list if you have trouble with the new relay support or configuration. The
relay only runs in Docker, so if you are running Acuparse in a VM or on a Raspberry Pi, you will need add support for
Docker if you wish to run the Relay locally.

Also in today's release, Tower Data is now reported during MQTT updates. Something a few users have been asking for.

On hardware side of things, Acuparse is removing installer support for Ubuntu 18 and adding support for Ubuntu 22.
The Docker images for Acuparse and MariaDB have discontinued support for `386`.

Thanks for using Acuparse and for your continued support! A special thanks to all those who have donated to the project.
Your support is greatly appreciated and means a ton. Without your ongoing support, there would be no need to continue
this project.

As always, come chat in Slack or drop a note on the email list if you have any questions or comments.
