---
layout: post
title: 'Introducing Acuparse Version 3'
date: 2020-10-25
---
After more than a year in development, Acuparse Version 3 is finally generally available.

There were many challenges to overcome throughout this release, the major challenge being acquiring and installing an Atlas
sensor for testing and new scripts for Docker based installs.

Acuparse Version 3 includes full support for the AcuRite Atlas sensors, including Lightning. See the [Release Notes](https://www.acuparse.com/releases/v3-0-0/)
for more details. Lightning comes from the Atlas and optionally one Lightning Tower. You can also replace the main Atlas
lightning sensor with a Tower.

This release also marks the first availability of the [Acuparse Docker](https://docs.acuparse.com/DOCKER) image.
Scripts are available to automate the install and running of Acuparse in Docker using Compose. See the [docs/DOCKER.md](https://docs.acuparse.com/DOCKER)
guide for detailed installation instructions for Docker based installs. Note, a special MariaDB image based
on the official MariaDB Docker image but built with Debian instead for running on Rasberian and ARM processors.
Both container images available at [Docker Hub](https://hub.docker.com/u/acuparse) for your manual install requirements.

This update includes many changes and the [Version 3 Upgrade Guide](https://docs.acuparse.com/updates/v3) should be
reviewed. As well, the V3 update script should be ran before attempting your upgrade. The repository release branch is
also changing as of this update from `master` to `stable`. The update script will attempt to make this change for you.

**Version 3 also moves to an API data export model. You'll need to update any Webcam scripts and other external scripts/tools
that uses the old HTML/JSON export urls.**

Also, shipping in this release is many user requested features and fixes. As well as, a change to update checking that
will report some basic install information back to the project for better ongoing support. See [Telemetry Guide](https://docs.acuparse.com/TELEMETRY)
for more details on how this works.

Official chat/discussions are also moving to Slack from the former Keybase/Gitter rooms.
- [Get Slack Invite](https://communityinviter.com/apps/acuparse/acuparse)

As always, please make a full backup before upgrading, to ensure you can recover from any unexpected failures.
If you need support, see the details on the homepage. There is also a dedicated V3 update channel on Slack. `#v3_update`
